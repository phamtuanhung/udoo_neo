/* authors              date(mm/dd/yyyy)        notes
 * hungpt               11/02/2018              Initial version
 */
#ifndef __AUTO_CONTROL_H__
#define __AUTO_CONTROL_H__


void udoo_auto_init();
void udoo_auto_go(int spedd);
void udoo_auto_go_foward(int speed);
void udoo_auto_go_backward(int speed);
void udoo_auto_turn_left(int speed);
void udoo_auto_turn_right(int speed);
void udoo_auto_stop();

#endif

