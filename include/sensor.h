/* authors              date(mm/dd/yyyy)        notes
 * hungpt               11/02/2018              Initial version
 */
#ifndef __SENSOR_H__
#define __SENSOR_H__

#include "fxas21002c.h"

void udoo_check_sensor();
sensor_data_t udoo_sensor_cal_accelero();
sensor_data_t udoo_sensor_cal_magneto();
#endif



