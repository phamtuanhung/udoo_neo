/* This file contain some macros of register used in FXAS21002C sensor.
 * FXAS21002C - Accelerometer and Magnetometer
*/

/* authors              date(mm/dd/yyyy)        notes
 * ThanhNT              02/10/2017              Initial version
*/

#ifndef __FXAS21002C_H__
#define __FXAS21002C_H__

// FXOS8700CQ I2C address
#define FXOS8700CQ_SLAVE_ADDR 0x1E // with pins SA0=0, SA1=0

#define FXOS8700CQ_STATUS 0x00
#define FXOS8700CQ_WHOAMI 0x0D
#define FXOS8700CQ_XYZ_DATA_CFG 0x0E
#define FXOS8700CQ_CTRL_REG1 0x2A
#define FXOS8700CQ_M_CTRL_REG1 0x5B
#define FXOS8700CQ_M_CTRL_REG2 0x5C
#define FXOS8700CQ_WHOAMI_VAL 0xC7

#define FXOS8700CQ_READ_LEN 13 //bytes
#define I2C_SLAVE       0x0703
#define I2C_SLAVE_FORCE       0x0706
#define I2C_FUNCS       0x0705  /* Get the adapter functionality */

typedef struct sensor_data_s 
{
   short x; 
   short y; 
   short z; 
} sensor_data_t;

#endif
