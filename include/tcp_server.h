/* authors              date(mm/dd/yyyy)        notes
 * hungpt               11/02/2018              Initial version
 */
#ifndef __TCP_SERVER_H__
#define __TCP_SERVER_H__

int udoo_tcp_server(int port);

#endif


