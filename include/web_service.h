/* authors              date(mm/dd/yyyy)        notes
 * hungpt               11/02/2018              Initial version
 */
#ifndef __WEB_SERVICE_H__
#define __WEB_SERVICE_H__

void udoo_web_service(int sock, char* message, unsigned int length);
void udoo_send_index(int sock);
void udoo_set_response(int sock, char* message);

#endif


