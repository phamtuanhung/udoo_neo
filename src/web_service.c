/* authors              date(mm/dd/yyyy)        notes
 * hungpt               11/02/2018              Initial version
 */

#include<stdio.h>
#include<string.h>    //strlen
#include<stdlib.h>    //strlen
#include<sys/socket.h>
#include<arpa/inet.h> //inet_addr
#include<unistd.h>    //write
#include<pthread.h> //for threading , link with lpthread
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <web_service.h>
#include <auto_control.h>

char * msg = "HTTP/1.0 200 Document follows\r\nServer: Udoo_Webserver\r\nContent-Type: text/html\r\n\r\n";

void udoo_web_service(int sock,char * message, unsigned int length)
{
    //printf("Client: %s\n",message);
    if (strstr(message, "GET /set") != NULL) {
       printf("Get set command\n");
       udoo_set_response(sock,message);
    }
    else
    {
        printf("Client: %s\n",message);	
        udoo_send_index(sock);
    }
}
void udoo_set_response(int sock, char* message)
{
    write(sock , msg , strlen(msg));
    if (strstr(message, "Forward") != NULL) {
       write(sock , "Going Forward", 13);
       udoo_auto_go_foward(80);       
    }
    else if (strstr(message, "Backward") != NULL) {
       write(sock , "Going Backward", 14);
       udoo_auto_go_backward(80);
    }
    else if (strstr(message, "Left") != NULL) {
       write(sock , "Turning Left", 12);
       udoo_auto_turn_left(80);
    }
    else if (strstr(message, "Right") != NULL) {
       write(sock , "Tunning Right", 13);
       udoo_auto_turn_right(80);
    }
    else if (strstr(message, "Stop") != NULL) {
       write(sock , "Stop", 4);
       udoo_auto_stop();
    }
    else
    {
        printf("%s\n",message);
    }
}
void udoo_send_index(int sock)
{
    int input_fd, output_fd;    /* Input and output file descriptors */
    ssize_t ret_in, ret_out;    /* Number of bytes returned by read() and write() */
    char buffer[200];      /* Character buffer */
 
    /* Create input file descriptor */
    input_fd = open ("./http/index.htm", O_RDONLY);
    if (input_fd == -1) {
            perror ("open");
    }
 
    /* Copy process */
    write(sock , msg , strlen(msg));
    while((ret_in = read (input_fd, &buffer, 200)) > 0){
        write(sock , buffer , ret_in);
    }
    /* Close file descriptors */
    close (input_fd);
} 
