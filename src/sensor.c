/* authors              date(mm/dd/yyyy)        notes
 * hungpt               11/02/2018              Initial version
 */

#include <stdio.h>
#include "fxas21002c.h"
#include "udoo_i2c_dev.h"
#include <unistd.h>
#include <string.h>
#include <pthread.h> //for threading , link with lpthread
#include <auto_control.h>

#define SENSITIVITY 0.244f
#define G_CONSTANT 9.81f

void * sensor_handler();
double getG(int value)
{
    double ret_val = ((value * SENSITIVITY)/1000)*G_CONSTANT;
    return ret_val;
}
int getH(int value)
{
    int ret_val = value/50;
    return ret_val;
}

void udoo_check_sensor()
{
    int *arg1;
    pthread_t sniffer_thread;
    printf("Start sensor thread");
    if( pthread_create( &sniffer_thread , NULL , sensor_handler, (void*) arg1) < 0)
    {
        perror("could not create thread");
    }
}
void *sensor_handler()
{
    int magneto, accelero;
    int i2cbus = 3; //for accelerometer and magnetometer
    char filename[30];
    char buffer[6];
    int fd = udoo_open_i2c_dev(i2cbus, filename, FXOS8700CQ_SLAVE_ADDR);
    sensor_data_t accelerometer;
    sensor_data_t magnetometer;
    /*
    while (1)
    {
        char control_data = 0;
        udoo_i2c_read_byte(fd, &control_data, 0x0E);
        printf("control data: %x\n", control_data);
        usleep(300*1000);
    }
    */
    udoo_i2c_disable_accel(fd);
    udoo_i2c_enable_accel(fd);
    while (1)
    {
        memset(buffer, 0, sizeof(buffer));
        udoo_i2c_read_accel(fd, buffer);

        accelerometer.x = (short)(((buffer[0] & 0xFF) << 8) | (buffer[1]) & 0xFF) >> 2;
        accelerometer.y = (short)(((buffer[2] & 0xFF) << 8) | (buffer[3]) & 0xFF) >> 2;
        accelerometer.z = (short)(((buffer[4] & 0xFF) << 8) | (buffer[5]) & 0xFF) >> 2;

        memset(buffer, 0, sizeof(buffer));
        udoo_i2c_read_mag(fd, buffer);
        magnetometer.x = (short)(((buffer[0] & 0xFF) << 8) | (buffer[1]) & 0xFF) >> 2;
        magnetometer.y = (short)(((buffer[2] & 0xFF) << 8) | (buffer[3]) & 0xFF) >> 2;
        magnetometer.z = (short)(((buffer[4] & 0xFF) << 8) | (buffer[5]) & 0xFF) >> 2;
        magneto = (magnetometer.z - magnetometer.x + 15)/4;
        accelero = getH(accelerometer.z);
        //printf("%d , %d , %d : ", getH(accelerometer.x), getH(accelerometer.y), getH(accelerometer.z));
        //printf(" %d , %d , %d\n", magnetometer.x, magnetometer.y, magnetometer.z);
        //printf("%d : %d\n", getH(accelerometer.z),  magneto);
        udoo_auto_go(accelero);
        usleep(30*1000);
    }
}
sensor_data_t udoo_sensor_cal_accelero()
{
    int i2cbus = 3; //for accelerometer and magnetometer
    char filename[30];
    char buffer[6];
    int fd = udoo_open_i2c_dev(i2cbus, filename, FXOS8700CQ_SLAVE_ADDR);
    sensor_data_t magnetometer;
    
    udoo_i2c_disable_accel(fd);
    udoo_i2c_enable_accel(fd);
    memset(buffer, 0, sizeof(buffer));
    udoo_i2c_read_accel(fd, buffer);

    accelerometer.x = (short)(((buffer[0] & 0xFF) << 8) | (buffer[1]) & 0xFF) >> 2;
    accelerometer.y = (short)(((buffer[2] & 0xFF) << 8) | (buffer[3]) & 0xFF) >> 2;
    accelerometer.z = (short)(((buffer[4] & 0xFF) << 8) | (buffer[5]) & 0xFF) >> 2;
	return accelerometer;
}
sensor_data_t udoo_sensor_cal_magneto()
{
    int i2cbus = 3; //for accelerometer and magnetometer
    char filename[30];
    char buffer[6];
    int fd = udoo_open_i2c_dev(i2cbus, filename, FXOS8700CQ_SLAVE_ADDR);
    sensor_data_t magnetometer;
    
    udoo_i2c_disable_accel(fd);
    udoo_i2c_enable_accel(fd);

    memset(buffer, 0, sizeof(buffer));
    udoo_i2c_read_mag(fd, buffer);
    magnetometer.x = (short)(((buffer[0] & 0xFF) << 8) | (buffer[1]) & 0xFF) >> 2;
    magnetometer.y = (short)(((buffer[2] & 0xFF) << 8) | (buffer[3]) & 0xFF) >> 2;
    magnetometer.z = (short)(((buffer[4] & 0xFF) << 8) | (buffer[5]) & 0xFF) >> 2;
	
	return magnetometer;
}
