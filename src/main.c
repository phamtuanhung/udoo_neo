#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <auto_control.h>
#include <sensor.h>
#include <tcp_server.h>

int main()
{
    udoo_auto_init();
    udoo_check_sensor();
    udoo_tcp_server(8888);
    return 0;
}
