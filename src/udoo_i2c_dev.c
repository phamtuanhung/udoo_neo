/* authors              date(mm/dd/yyyy)        notes
 * ThanhNT              02/10/2017              Initial version
 */
#include "udoo_i2c_dev.h"
#include "fxas21002c.h"
#include <string.h>
int udoo_open_i2c_dev(const int i2cbus, char *filename, int dev_addr)
{
    int fd;

    sprintf(filename, "/dev/i2c-%d", i2cbus);
    fd = open(filename, O_RDWR);
    if (fd < 0)
    {
        printf("ERR 0: %s\n", filename);
        return -1;
    }

    if (ioctl(fd, I2C_SLAVE_FORCE, dev_addr) < 0) {
        printf("ERR 2\n");
        return -1;
    }

    return fd;
}

int udoo_i2c_read_byte(const int fd, char* buffer, int register_addr)
{
    udoo_i2c_cmd_t cmd;
    cmd.read_write = UDOO_I2C_READ;
    cmd.register_addr = register_addr;
    cmd.size = 2;
    cmd.data = buffer;
    return ioctl(fd, I2C_SMBUS, &cmd);
}

int udoo_i2c_read_accel(const int fd, char buffer[])
{
    int reg_addr=0x1;
    int idx=0;
    char tmp_buffer;
    for (reg_addr=0x1,idx=0; reg_addr <= 0x06, idx<6; reg_addr++, idx++)
    {
        udoo_i2c_read_byte(fd, &tmp_buffer, reg_addr);
        buffer[idx] = tmp_buffer;
    }
}
int udoo_i2c_read_mag(const int fd, char buffer[])
{
    int reg_addr=0x33;
    int idx=0;
    char tmp_buffer;
    for (reg_addr=0x33,idx=0; reg_addr <= 0x38, idx<6; reg_addr++, idx++)
    {
        udoo_i2c_read_byte(fd, &tmp_buffer, reg_addr);
        buffer[idx] = tmp_buffer;
    }
}
int udoo_i2c_write_byte(const int fd, char* buffer, int register_addr)
{
    udoo_i2c_cmd_t cmd;
    cmd.read_write = UDOO_I2C_WRITE;
    cmd.register_addr = register_addr;
    cmd.size = 2;
    cmd.data = buffer;
    return ioctl(fd, I2C_SMBUS, &cmd);
}
int udoo_i2c_enable_accel(const int fd)
{
    //char enable=0x13;
    char enable=0x11;
    char full_scale=0x00;
    udoo_i2c_write_byte(fd, &enable, FXOS8700CQ_CTRL_REG1);
    udoo_i2c_write_byte(fd, &full_scale, FXOS8700CQ_XYZ_DATA_CFG);
}
int udoo_i2c_disable_accel(const int fd)
{
    char enable=0x0;
    return udoo_i2c_write_byte(fd, &enable, FXOS8700CQ_CTRL_REG1);
}
