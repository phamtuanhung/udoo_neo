/* authors              date(mm/dd/yyyy)        notes
 * hungpt               11/02/2018              Initial version
 */

#include<stdio.h>
#include<string.h>    //strlen
#include<stdlib.h>    //strlen
#include<unistd.h>    //write
#include<fxas21002c.h>
#include<web_service.h>

sensor_data_t base_accelero;
sensor_data_t base_magneto;

void udoo_auto_init()
{
    system("echo 0 > /sys/class/pwm/pwmchip0/export");
    system("echo 1000000 > /sys/class/pwm/pwmchip0/pwm0/period");
    system("echo 0 > /sys/class/pwm/pwmchip0/pwm0/duty_cycle");
    system("echo 1 > /sys/class/pwm/pwmchip0/pwm0/enable");
    system("echo out > /gpio/pin28/direction");
    system("echo out > /gpio/pin29/direction");
    system("echo out > /gpio/pin32/direction");
    system("echo out > /gpio/pin33/direction");
	base_accelero = udoo_sensor_cal_accelero();
	base_magneto = udoo_sensor_cal_magneto();
}
void udoo_auto_go(int speed)
{
    char cmd[200];
	speed = speed - base_accelero.z;
    if (speed<0)
    {
        speed = 0 - speed;
		system("echo 1 > /gpio/pin33/value");
    }
	else
	{
		system("echo 0 > /gpio/pin33/value");
	}
    sprintf(cmd, "echo %d000 > /sys/class/pwm/pwmchip0/pwm0/duty_cycle", speed);
    system(cmd);
}

void udoo_auto_go_foward(int speed)
{
    system("echo 1 > /gpio/pin33/value");
}
void udoo_auto_go_backward(int speed)
{
    system("echo 0 > /gpio/pin33/value");
}
void udoo_auto_turn_left(int speed)
{
    system("echo 1 > /gpio/pin28/value");
    system("echo 0 > /gpio/pin29/value");
}
void udoo_auto_turn_right(int speed)
{
    system("echo 1 > /gpio/pin28/value");
    system("echo 0 > /gpio/pin29/value");
}

void udoo_auto_stop()
{
    system("echo 0 > /sys/class/pwm/pwmchip0/pwm0/duty_cycle");
    system("echo 0 > /gpio/pin28/value");
    system("echo 0 > /gpio/pin28/value");
    system("echo 0 > /gpio/pin33/value");
}
