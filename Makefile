.PHONY: all clean

CROSS_COMPILE=gcc -pthread
C_SRC=main.c web_service.c tcp_server.c auto_control.c sensor.c udoo_i2c_dev.c
C_DIR=.
CFLAGS += -std=gnu99
CFLAGS += -I./include

PROGRAM=auto_control
all: $(PROGRAM)

$(PROGRAM): $(C_SRC:.c=.o)
	$(CROSS_COMPILE) -o $@ $^ $(LIBS)

$(C_SRC:.c=.o): %.o: $(C_DIR)/src/%.c
	$(CROSS_COMPILE) -c -o $@ $^ $(CFLAGS) $(LIBS)

clean:
	rm -rf *.o
	rm -rf $(PROGRAM)
